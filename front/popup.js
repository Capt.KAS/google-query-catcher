document.addEventListener('DOMContentLoaded', function() {

  const checkPageSwitch = document.getElementById('switch');
  

  // Getting activated parameter
  chrome.storage.sync.get("gqc-activated", function(data) {

    // Check if activated before
    if (data["gqc-activated"] === true) {
      checkPageSwitch.setAttribute("checked", true);
    }

    checkPageSwitch.addEventListener('click', function() {
      if (checkPageSwitch.checked) {
        // Setting activated parameter
        chrome.storage.sync.set({"gqc-activated": true}, function() {
          if (chrome.runtime.lastError) {
            console.error("Error setting gqc-activated paramerer");
          }
        });
        chrome.tabs.executeScript(null, { file: "scrap.js" });
      } else {
        chrome.storage.sync.set({"gqc-activated": false}, function() {
          if (chrome.runtime.lastError) {
            console.error("Error setting gqc-activated paramerer");
          }
        });
      }

    }, false);
  });
}, false);