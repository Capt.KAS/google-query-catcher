chrome.storage.sync.get("gqc-activated", function(data) {
  if (data["gqc-activated"] === true) {

    /*
    *  Get informations from page
    */
    console.info("GQC => Scrapping page ...");

    const inputNode = document.querySelectorAll("input.gsfi")[0];
    const resultNodes = Array.from(document.getElementsByClassName("g"));
    const pagerNodes = Array.from(document.getElementsByTagName("tbody")[0].getElementsByTagName("td"));

    const search = inputNode.getAttribute("value");

    const resultNodesCleaned = resultNodes.filter(function(elem) {
      return elem.classList.length === 1;
    })
    const results = resultNodesCleaned.map(function(elem) {
      const header = elem.getElementsByClassName("yuRUbf")[0];
      const info = elem.getElementsByClassName("IsZvec")[0];

      return {
        link: header.firstChild.getAttribute("href"),
        name: header.firstChild.getElementsByTagName("h3")[0].textContent,
        description: info.textContent,
      }
    });

    pagerNodes.shift();
    pagerNodes.pop();
    const currentPageNode = pagerNodes.find(function(element) {
      return element.classList.contains("YyVfkd");
    })
    const page = currentPageNode === undefined ? "1" : currentPageNode.textContent;

    console.info("GQC => Scrapping done");

    /*
    *  Send informations to server
    */

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:8000/query";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    var data = JSON.stringify({
      search,
      results,
      page
    });
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        console.info("GQC => Information received by server");
      }
  };
    xhr.send(data);

    console.info("GQC => Information sent to server");

  }
});



