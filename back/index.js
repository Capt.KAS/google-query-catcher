const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const compression = require("compression");
const helmet = require("helmet");
const bodyparser = require("body-parser");

const app = express();

app.use(morgan("common"));
app.use(helmet());
app.use(cors({
  origin: ["chrome-extension"],
  methods: ["GET", "POST"],
  allowedHeaders: ["Content-Type"]
}));
app.use(compression());
app.use(bodyparser.json());

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.get("/", function(req, res) {
  res.send("API is alive !")
});

app.post("/query", function(req, res) {
  console.log(req.body);
  res.send("OK");
});

app.listen(8000, function() {
  console.log("=> API is running.");
});

exports.default = app;
