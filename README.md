# Google query catcher

Chrome plugin and server for catching google requests

# Initialization

## Front (plugin)

In your chrome browser go to chrome://extensions and click on "Load unpacked", then select the front folder inside the repository.
The plugin icon should appear on your appbar, click on the plugin then activate the switch, the plugin should start catching google requests.

## Back

Navigate to the back folder in your CLI and launch
`> npm install`
`> npm start`

